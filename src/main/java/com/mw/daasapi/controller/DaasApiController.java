package com.mw.daasapi.controller;

import com.mw.daasapi.entity.CdlCel;
import com.mw.daasapi.repository.CdlCelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DaasApiController {

    @Autowired
    private CdlCelRepository cdlCelRepository;

    @PostMapping("/cdlcel")
    public CdlCel saveCdlCel(@RequestBody CdlCel c){
        return cdlCelRepository.save(c);
    }

    @GetMapping("/cdlcel/{id}")
    public CdlCel getCdlCel(@PathVariable("id") String ifa){
        return cdlCelRepository.get(ifa);

    }

    @DeleteMapping("/cdlcel/{id}")
    public String deleteCdlCel(@PathVariable("id") String ifa){
        return cdlCelRepository.delete(ifa);

    }

    @PutMapping("/cdlcel/{id}")
    public String updatedCdlCel(@PathVariable("id") String ifa,@RequestBody CdlCel c){
        return cdlCelRepository.update(ifa,c);

    }



}
