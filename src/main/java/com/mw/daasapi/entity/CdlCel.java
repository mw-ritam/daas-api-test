package com.mw.daasapi.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "test_cdlcel")
public class CdlCel {

    @DynamoDBHashKey
    private String ifa;

    @DynamoDBAttribute
    private String cdl_city;

    @DynamoDBAttribute
    private String cel_city;

    @DynamoDBAttribute
    private Cdl cdl_details;

}
