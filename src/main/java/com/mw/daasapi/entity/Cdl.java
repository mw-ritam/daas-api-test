package com.mw.daasapi.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBDocument
public class Cdl {

    @DynamoDBAttribute
    private String zip;

    @DynamoDBAttribute
    private Double lat;

    @DynamoDBAttribute
    private Double lon;
}
