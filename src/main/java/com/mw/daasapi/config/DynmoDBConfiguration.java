package com.mw.daasapi.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.auth.ProcessCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.auth.profile.ProfilesConfigFile;
import com.amazonaws.auth.profile.internal.ProfileAssumeRoleCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.Credentials;
import java.util.Properties;
import java.util.Scanner;

@Configuration
public class DynmoDBConfiguration {

    @Bean
    public DynamoDBMapper dynamoDBMapper(){

        return new DynamoDBMapper(amazonDynamoDBClientV1());
    }

    private AmazonDynamoDB amazonDynamoDBClientV1() {
        return AmazonDynamoDBClientBuilder
                .standard()
                .withRegion("us-east-1")
                .withCredentials(
                        new ProfileCredentialsProvider("mw-dev")
                ).build();

                /*.withRegion("us-east-1")
                .withCredentials(
                        new ProfileCredentialsProvider("mwdata-dynamodb")
                ).build();
                */

                /*
                .withRegion("us-east-1")
                .withCredentials(
                        new AWSStaticCredentialsProvider(
                                new DefaultAWSCredentialsProviderChain().getCredentials()
                        ))
                .build();
                */
    }

    private AmazonDynamoDB amazonDynamoDBClientV2() {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter mfa code: ");
        String mfaCode = sc.nextLine();

        String clientRegion = "us-east-1";
        String roleARN = "arn:aws:iam::120859525614:role/engineer";
        String roleSessionName = "mw-dev";
        String serialNumber = "arn:aws:iam::139138045055:user/ritam@mobilewalla.com";

        AWSSecurityTokenService stsClient = AWSSecurityTokenServiceClientBuilder.standard()
                .withCredentials(new ProfileCredentialsProvider())
                .withRegion(clientRegion)
                .build();

        AssumeRoleRequest roleRequest = new AssumeRoleRequest()
                .withRoleArn(roleARN)
                .withSerialNumber(serialNumber)
                .withTokenCode(mfaCode)
                .withRoleSessionName(roleSessionName);

        AssumeRoleResult roleResponse = stsClient.assumeRole(roleRequest);
        Credentials sessionCredentials = roleResponse.getCredentials();

        BasicSessionCredentials awsCredentials = new BasicSessionCredentials(
                sessionCredentials.getAccessKeyId(),
                sessionCredentials.getSecretAccessKey(),
                sessionCredentials.getSessionToken());

        AmazonDynamoDB dbClient = AmazonDynamoDBClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();

    }

}

   /* String clientRegion = "us-east-1";
    String roleARN = "arn:aws:iam::120859525614:role/engineer";
    String roleSessionName = "mw-dev";
    AWSSecurityTokenService stsClient = AWSSe.standard()
            .withCredentials(new ProfileCredentialsProvider())
            .withRegion(clientRegion)
            .build();
    ProfileAssumeRoleCredentialsProvider roleRequest = new AssumeRoleRequest()
            .withRoleArn(roleARN)
            .withSerialNumber(serialNumber)
            .withTokenCode(mfaCode)
            .withRoleSessionName(roleSessionName);
    AssumeRoleResult roleResponse = stsClient.assumeRole(roleRequest);
    Credentials sessionCredentials = roleResponse.getCredentials();
    BasicSessionCredentials awsCredentials = new BasicSessionCredentials(
            sessionCredentials.getAccessKeyId(),
            sessionCredentials.getSecretAccessKey(),
            sessionCredentials.getSessionToken());
    AmazonDynamoDB dbClient = AmazonDynamoDBClientBuilder
            .standard()
            .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
            .build();
*/
/*

dynamoDB_role_arn = arn:aws:iam::361166629815:role/mwdata-dynamodb
dynamoDB_profile_name = "mwdata-dynamodb"


 */

    /*static AWSStaticCredentialsProvider getDynamoDBCredentials() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("/root/airflow/dags/mw_emrcluster_indexer_dag/conf/config.properties"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String arn = "arn:aws:iam::361166629815:role/mwdata-dynamodb" //Change
        Assume assumeRole = new AssumeRoleRequest().withRoleArn(arn)
                .withRoleSessionName("mw-emrcluster-indexer-java-sdk");
        AWSSecurityTokenService sts = AWSSecurityTokenServiceClientBuilder.standard().withRegion("us-east-1").build();
        Credentials credentials = sts.assumeRole(assumeRole).getCredentials();
        BasicSessionCredentials sessionCredentials = new BasicSessionCredentials(credentials.getAccessKeyId(),
                credentials.getSecretAccessKey(), credentials.getSessionToken());
        return new AWSStaticCredentialsProvider(sessionCredentials);
    }*/

    /*
    .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration(
                                "dynamodb.us-east-1.amazonaws.com",
                                "us-east-1"
                        )
                )
     */
    /*
     return AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new DefaultAWSCredentialsProviderChain())
                .withRegion(Regions.fromName(region))
                .build();
     */
