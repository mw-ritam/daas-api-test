package com.mw.daasapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DaasApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DaasApiApplication.class, args);
	}

}
