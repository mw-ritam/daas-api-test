package com.mw.daasapi.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.mw.daasapi.entity.CdlCel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CdlCelRepository {

    @Autowired
    private DynamoDBMapper dynamoDBMapper;

    public CdlCel save(CdlCel c){
        dynamoDBMapper.save(c);
        return c;

    }

    public CdlCel get(String ifa){
        return dynamoDBMapper.load(CdlCel.class,ifa);
    }


    public String delete(String ifa){
       CdlCel c = dynamoDBMapper.load(CdlCel.class,ifa);
       dynamoDBMapper.delete(c);
        return "CdlCel Deleted!";
    }


    public String update(String ifa,CdlCel c){
        dynamoDBMapper.save(c, new DynamoDBSaveExpression()
                .withExpectedEntry("ifa",
                new ExpectedAttributeValue( new AttributeValue().withS(ifa)  )
                )
        );
        return ifa;
    }

}
